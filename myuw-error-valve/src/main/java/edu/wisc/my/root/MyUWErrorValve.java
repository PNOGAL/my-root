package edu.wisc.my.root;

import java.io.IOException;
import java.io.InputStream;

import org.apache.catalina.Valve;
import org.apache.catalina.connector.Request;
import org.apache.catalina.connector.Response;
import org.apache.catalina.valves.ErrorReportValve;


public class MyUWErrorValve extends ErrorReportValve implements Valve {
  
  private static String ERROR_PAGE_LOCATION="/500.html";
  private static String AUTHORIZATION_ERROR_LOCATION="/403.html";
  private static String MISSING_PAGE_LOCATION="/404.html";
  
  @Override
  protected void report(Request request, Response response, Throwable throwable) {
    int statusCode = response.getStatus();

    // Do nothing on a 1xx, 2xx and 3xx status
    // Do nothing if anything has been written already
    // Do nothing if the response hasn't been explicitly marked as in error
    //    and that error has not been reported.
    if (statusCode < 400 || response.getContentWritten() > 0 || !response.setErrorReported()) {
        return;
    }
    final String page;
    InputStream in = null;
    try {
      switch(statusCode){
      case 403:
    	  page = AUTHORIZATION_ERROR_LOCATION;
    	  break;
      case 404:
    	  page=MISSING_PAGE_LOCATION;
    	  break;
      default:
    	  page=ERROR_PAGE_LOCATION;
      }
      
      try {
        in = MyUWErrorValve.class.getResourceAsStream(page);
        if(in == null) {
          //page missing or something, revert to simpler times
          response.getWriter().write("<html><head><title>Error: "+ statusCode + "</title></head><body>"+ statusCode + "</body><html>");
        }
        int c;
        while ((c = in.read()) != -1) {
          response.getWriter().write(c);
        }
        response.flushBuffer();
      } finally {
        if(in != null) {
          in.close();
        }
        response.finishResponse();
      }
    } catch (IOException e) {
      super.report(request, response, throwable);
    }
  }
  
}