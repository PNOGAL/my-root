# MyUW root webapp and Tomcat error valve

There are two modules here: a webapp to serve as the root web application
context and plugin to customize Tomcat's error handling.

## MyUW root webapp

This is a small webapp to serve as the root, default servlet application in MyUW
Tomcat. It contains the error pages to fall back upon when another servlet
context does not handle the request.

### Development of the root webapp

From the root directory, run `npm start`. The web app will run at
<http://localhost:8080> and will show the 404 page by
default.

## MyUW Tomcat error valve

Customizes Tomcat to use the error pages in the custom root webapp.

## Releasing

The root webapp and the valve are versioned and released together.

From the root directory of this project

```
mvn release:prepare
mvn release:perform
```

When you release a new version of this project, you'll publish both a root
webapp .war and a Tomcat error valve .jar.

For these to be actually used in MyUW, update the tier-specific ears to
[reference the new root webapp version][MyUW ear use of root webapp],
and update the Tomcat container to
[use the new valve version][MyUW Tomcat use of valve].

[MyUW ear use of root webapp]: https://git.doit.wisc.edu/myuw-overlay/myuw-ear/-/blob/qa/pom.xml#L54
[MyUW Tomcat use of valve]: https://git.doit.wisc.edu/myuw/tomcat7-java8/-/blob/master/Dockerfile#L8

